import React from 'react'
import contactx2 from '../assets/contactx2.png'

function UserDetails({ details: { productions } }) {
    return (
        <div className='details-container'>
            {productions.map((prod, i) => {
                return (
                    <div key={i} className='production-row'>
                        <div className='row'>
                            <div className='type'><span>{prod.type}</span> - <span>{prod.title}</span></div>
                            <div className='year'>{prod.year}</div>
                        </div>
                        <div className='row'>
                            <div className='description'>{prod.description}</div>
                            <div className='description'>{prod.company}</div>
                        </div>
                    </div>
                )
            })}
            <div className='extra'>
                <div className='connections'>MUTUAL CONNECTIONS</div>
                <div className='avatars'>
                    <img src={contactx2} alt="contact avatar" />
                    <img src={contactx2} alt="contact avatar" />
                    <img src={contactx2} alt="contact avatar" />
                    <img src={contactx2} alt="contact avatar" />
                    <img src={contactx2} alt="contact avatar" />
                    <div className="count">3+</div>
                </div>
                <div className='credits'>RELEVANT CREDITS <span className="number">12</span></div>
            </div>
        </div>
    )
}

export default UserDetails