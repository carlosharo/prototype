import React from 'react'

function UserProfile({ person: { avatarImg, matchPercent, name, city, description } }) {
    return (
        <div className='profile-container'>
            <div className="close">x</div>
            <div className="avatar">
                <img src={avatarImg} alt="person avatar" height="85" width="85" />
            </div>
            <div className="match">${matchPercent} MATCH</div>
            <div className="name">{name}</div>
            <div className="city">{city}</div>
            <div className="self-description">{description}</div>
            <div className="buttons">
                <div>X PASS</div>
                <div>+ SHORTLIST</div>
            </div>
        </div>
    )
}

export default UserProfile