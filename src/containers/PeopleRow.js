import React, { Component } from 'react'
import UserProfile from '../components/UserProfile'
import UserDetails from '../components/UserDetails'
import { user } from '../assets/data'
import Avatarx2 from '../assets/Avatarx2.png'

const productions = [user.production, user.production, user.production]
const details = {
    productions
}
const person = {
    avatarImg: Avatarx2,
    matchPercent: '70%',
    name: 'Richard Smith',
    city: 'Los Angeles, CA',
    description: 'I’m an experienced editor and I bring talent and a good attitude to the edit.'
}

class PeopleRow extends Component {
    render() {
        return (
            <div className='card'>
                <div className="container">
                    <UserProfile person={person} />
                    <UserDetails details={details} />
                </div>
            </div>
        )
    }
}

export default PeopleRow