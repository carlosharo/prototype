import React from 'react';
import './styles/App.scss';
import PeopleRow from './containers/PeopleRow'

function App() {
  return (
    <div className="container">
      <PeopleRow />
    </div>
  );
}

export default App;
